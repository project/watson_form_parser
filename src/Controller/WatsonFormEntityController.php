<?php

namespace Drupal\watson_form_parser\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\watson_form_parser\Entity\WatsonFormEntityInterface;

/**
 * Class WatsonFormEntityController.
 *
 *  Returns responses for Watson form routes.
 */
class WatsonFormEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Watson form  revision.
   *
   * @param int $watson_form_entity_revision
   *   The Watson form  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($watson_form_entity_revision) {
    $watson_form_entity = $this->entityManager()->getStorage('watson_form_entity')->loadRevision($watson_form_entity_revision);
    $view_builder = $this->entityManager()->getViewBuilder('watson_form_entity');

    return $view_builder->view($watson_form_entity);
  }

  /**
   * Page title callback for a Watson form  revision.
   *
   * @param int $watson_form_entity_revision
   *   The Watson form  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($watson_form_entity_revision) {
    $watson_form_entity = $this->entityManager()->getStorage('watson_form_entity')->loadRevision($watson_form_entity_revision);
    return $this->t('Revision of %title from %date', ['%title' => $watson_form_entity->label(), '%date' => format_date($watson_form_entity->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Watson form .
   *
   * @param \Drupal\watson_form_parser\Entity\WatsonFormEntityInterface $watson_form_entity
   *   A Watson form  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(WatsonFormEntityInterface $watson_form_entity) {
    $account = $this->currentUser();
    $langcode = $watson_form_entity->language()->getId();
    $langname = $watson_form_entity->language()->getName();
    $languages = $watson_form_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $watson_form_entity_storage = $this->entityManager()->getStorage('watson_form_entity');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $watson_form_entity->label()]) : $this->t('Revisions for %title', ['%title' => $watson_form_entity->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all watson form revisions") || $account->hasPermission('administer watson form entities')));
    $delete_permission = (($account->hasPermission("delete all watson form revisions") || $account->hasPermission('administer watson form entities')));

    $rows = [];

    $vids = $watson_form_entity_storage->revisionIds($watson_form_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\watson_form_parser\WatsonFormEntityInterface $revision */
      $revision = $watson_form_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $watson_form_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.watson_form_entity.revision', [
            'watson_form_entity' => $watson_form_entity->id(),
            'watson_form_entity_revision' => $vid,
          ]));
        }
        else {
          $link = $watson_form_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.watson_form_entity.translation_revert', [
                'watson_form_entity' => $watson_form_entity->id(),
                'watson_form_entity_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.watson_form_entity.revision_revert', [
                'watson_form_entity' => $watson_form_entity->id(),
                'watson_form_entity_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.watson_form_entity.revision_delete', [
                'watson_form_entity' => $watson_form_entity->id(),
                'watson_form_entity_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['watson_form_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
