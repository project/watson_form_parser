<?php

namespace Drupal\watson_form_parser\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Watson form entity.
 *
 * @ingroup watson_form_parser
 *
 * @ContentEntityType(
 *   id = "watson_form_entity",
 *   label = @Translation("Watson form"),
 *   label_collection = @Translation("Watson forms"),
 *   label_singular = @Translation("Watson form"),
 *   label_plural = @Translation("Watson forms"),
 *   handlers = {
 *     "storage" = "Drupal\watson_form_parser\WatsonFormEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\watson_form_parser\WatsonFormEntityListBuilder",
 *     "views_data" = "Drupal\watson_form_parser\Entity\WatsonFormEntityViewsData",
 *     "translation" = "Drupal\watson_form_parser\WatsonFormEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\watson_form_parser\Form\WatsonFormEntityForm",
 *       "add" = "Drupal\watson_form_parser\Form\WatsonFormEntityForm",
 *       "edit" = "Drupal\watson_form_parser\Form\WatsonFormEntityForm",
 *       "delete" = "Drupal\watson_form_parser\Form\WatsonFormEntityDeleteForm",
 *     },
 *     "access" = "Drupal\watson_form_parser\WatsonFormEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\watson_form_parser\WatsonFormEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "watson_form_entity",
 *   data_table = "watson_form_entity_field_data",
 *   revision_table = "watson_form_entity_revision",
 *   revision_data_table = "watson_form_entity_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer watson form entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/watson_form/{watson_form_entity}",
 *     "add-form" = "/admin/structure/watson_form/add",
 *     "edit-form" = "/admin/structure/watson_form/{watson_form_entity}/edit",
 *     "delete-form" = "/admin/structure/watson_form/{watson_form_entity}/delete",
 *     "version-history" = "/admin/structure/watson_form/{watson_form_entity}/revisions",
 *     "revision" = "/admin/structure/watson_form/{watson_form_entity}/revisions/{watson_form_entity_revision}/view",
 *     "revision_revert" = "/admin/structure/watson_form/{watson_form_entity}/revisions/{watson_form_entity_revision}/revert",
 *     "revision_delete" = "/admin/structure/watson_form/{watson_form_entity}/revisions/{watson_form_entity_revision}/delete",
 *     "translation_revert" = "/admin/structure/watson_form/{watson_form_entity}/revisions/{watson_form_entity_revision}/revert/{langcode}",
 *     "collection" = "/admin/content/watson_forms",
 *   },
 *   field_ui_base_route = "watson_form_entity.settings"
 * )
 */
class WatsonFormEntity extends RevisionableContentEntityBase implements WatsonFormEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the
    // watson_form_entity owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormHtml() {
    return $this->get('html');
  }

  /**
   * {@inheritdoc}
   */
  public function getAdminDescription() {
    return $this->get('admin_description');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormText() {
    return $this->get('form_text');
  }

  /**
   * {@inheritdoc}
   */
  public function getReferencedWebform() {
    return $this->get('referenced_webform')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Watson form entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Watson form entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Watson form is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['form_text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Webform Description'))
      ->setDescription(t('The description that will be displayed on the Webform above any form fields. Consider including the title to be displayed.<br/>After initial creation, this value should be edited from the Webform UI.'))
      ->setDisplayOptions('form', [
        'weight' => -2,
        'type' => 'text_textarea',
      ])
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['admin_description'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Webform Admin Description'))
      ->setDescription(t('Administrative description of the Webform.'))
      ->setDisplayOptions('form', [
        'weight' => -3,
      ])
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['html'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Base Form HTML'))
      ->setDescription(t('The raw HTML to be parsed.'))
      ->setDisplayOptions('form', [
        'weight' => -3,
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->addConstraint('SingleForm');

    $fields['referenced_webform'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Webform'))
      ->setDescription(t('The Webform created by the raw HTML.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'webform')
      ->setSetting('handler', 'default:webform')
      ->setTranslatable(FALSE)
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'webform_entity_reference_link',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -2,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

}
