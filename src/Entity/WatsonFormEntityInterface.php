<?php

namespace Drupal\watson_form_parser\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Watson form entities.
 *
 * @ingroup watson_form_parser
 */
interface WatsonFormEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Watson form name.
   *
   * @return string
   *   Name of the Watson form.
   */
  public function getName();

  /**
   * Sets the Watson form name.
   *
   * @param string $name
   *   The Watson form name.
   *
   * @return \Drupal\watson_form_parser\Entity\WatsonFormEntityInterface
   *   The called Watson form entity.
   */
  public function setName($name);

  /**
   * Gets the Watson form creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Watson form.
   */
  public function getCreatedTime();

  /**
   * Sets the Watson form creation timestamp.
   *
   * @param int $timestamp
   *   The Watson form creation timestamp.
   *
   * @return \Drupal\watson_form_parser\Entity\WatsonFormEntityInterface
   *   The called Watson form entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Watson form published status indicator.
   *
   * Unpublished Watson form are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Watson form is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Watson form.
   *
   * @param bool $published
   *   TRUE to set this Watson form to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\watson_form_parser\Entity\WatsonFormEntityInterface
   *   The called Watson form entity.
   */
  public function setPublished($published);

  /**
   * Gets the Watson form revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Watson form revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\watson_form_parser\Entity\WatsonFormEntityInterface
   *   The called Watson form entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Watson form revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Get the Admin Description from the admin_description field.
   *
   * @return string
   *   The admin description.
   */
  public function getAdminDescription();

  /**
   * Get the Form text from the form_text field.
   *
   * @return string
   *   The form text.
   */
  public function getFormText();

  /**
   * Get the HTML from the html field.
   *
   * @return string
   *   The string of HTML.
   */
  public function getFormHtml();

  /**
   * Get the referenced webform.
   *
   * @return Drupal\webform\Entity\Webform
   *   The referenced Webform entity.
   */
  public function getReferencedWebform();

  /**
   * Sets the Watson form revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\watson_form_parser\Entity\WatsonFormEntityInterface
   *   The called Watson form entity.
   */
  public function setRevisionUserId($uid);

}
