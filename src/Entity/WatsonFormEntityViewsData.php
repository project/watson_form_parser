<?php

namespace Drupal\watson_form_parser\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Watson form entities.
 */
class WatsonFormEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
