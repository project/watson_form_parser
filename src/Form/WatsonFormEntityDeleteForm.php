<?php

namespace Drupal\watson_form_parser\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Watson form entities.
 *
 * @ingroup watson_form_parser
 */
class WatsonFormEntityDeleteForm extends ContentEntityDeleteForm {


}
