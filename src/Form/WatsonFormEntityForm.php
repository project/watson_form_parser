<?php

namespace Drupal\watson_form_parser\Form;

use Drupal\webform\Entity\Webform;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\webform\Plugin\WebformHandlerManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Watson form edit forms.
 *
 * @ingroup watson_form_parser
 */
class WatsonFormEntityForm extends ContentEntityForm {

  /**
   * String of raw HTML.
   *
   * @var string
   */
  private $html;

  /**
   * Title of Webform.
   *
   * @var string
   */
  private $title;

  /**
   * Administrative description.
   *
   * @var string
   */
  private $adminDescription;

  /**
   * Markup text for the Webform.
   *
   * @var string
   */
  private $formText;

  /**
   * Markup text format for the Webform.
   *
   * @var string
   */
  private $formTextFormat;

  /**
   * Generated Webform ID.
   *
   * @var string
   */
  private $webformId;

  /**
   * The array of everything before the layout containers.
   *
   * @var array
   */
  private $preArray;

  /**
   * Array of form elements that are after the initial preArray elements.
   *
   * @var array
   */
  private $elementArray;

  /**
   * Fully built form array.
   *
   * @var array
   */
  private $formArray;

  /**
   * The old form array flattened for easier comparison.
   *
   * @var array
   */
  private $oldFormFlat;

  /**
   * The new form array flattened for easier comparison.
   *
   * @var array
   */
  private $newFormFlat;

  /**
   * The xpath object.
   *
   * @var DOMXpath
   */
  private $xpathObject;

  /**
   * The handler manager.
   *
   * @var \Drupal\webform\Plugin\WebformHandlerManagerInterface
   */
  protected $handlerManager;

  /**
   * The handler array.
   *
   * @var array
   */
  private $handlerArray;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The account proxy/current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\webform\Plugin\WebformHandlerManagerInterface $handler_manager
   *   The handler manager service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The account proxy service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, WebformHandlerManagerInterface $handler_manager, AccountProxyInterface $current_user) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->handlerManager = $handler_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.webform.handler'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\watson_form_parser\Entity\WatsonFormEntity */
    $form = parent::buildForm($form, $form_state);

    $form['referenced_webform']['#disabled'] = TRUE;
    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#weight' => 10,
      ];
      $form['form_text']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Today we're going to create a webform.
    $this->title = $entity->getName();
    // Use the title of the new entity as the ID of the webform.
    $this->webformId = str_replace(' ', '_', strtolower($this->title));

    // Get the admin description and form text.
    $this->adminDescription = $entity->getAdminDescription()->value;

    $this->formText = $entity->getFormText()->value;

    $this->formTextFormat = $this->getFormTextFormat($form['form_text']);

    // This is where the magic HTML -> YAML happens.
    $this->parseHtml($this->getHtml());

    // Begin creation of a webform.
    $webform = $this->createWebform();

    // Once we've created or updated a webform, we should have the ID.
    $webform_id = $webform->id();

    // Create a reference to the baby webform so it knows who its mother is.
    $entity->referenced_webform->target_id = $webform_id;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime(REQUEST_TIME);
      $entity->setRevisionUserId($this->currentUser->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }

    // Let's go ahead and save our new entity.
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Watson form.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Watson form.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.webform.canonical', ['webform' => $webform_id]);
  }

  /**
   * Parses the HTML into YAML.
   *
   * @param string $html
   *   The raw HTML from the entity form.
   */
  private function parseHtml($html) {
    // Get some cleaned up HTML.
    $clean_html = $this->cleanHtml($html);
    $dom = new \DOMDocument();
    $dom->loadHTML($clean_html, LIBXML_HTML_NOIMPLIED);
    $eles = $dom->getElementsByTagName('*');
    $this->xpathObject = new \DOMXPath($dom);

    foreach ($eles as $ele) {
      if ($ele->hasAttributes()) {
        $attributes = $ele->attributes;
        $prev_name = $ele->previousSibling->nodeName ?? FALSE;
        $next_name = $ele->nextSibling->nodeName ?? FALSE;
        $type = $attributes->getNamedItem('type')->value ?? NULL;
        // Radios are their own challenge, so they get their own method.
        if ($type === 'radio' && $ele === $ele->parentNode->firstChild) {
          $this->getRadioAttributes($ele);
        }
        if ($type === 'checkbox') {
          $this->getCheckboxItems($ele);
        }
        // Lots of checking here to make sure we don't overdo the radio stuff.
        // @TODO: See if there's a way to clean this up a bit?
        elseif (($type !== 'radio' &&
          $prev_name !== 'input' &&
          $next_name !== 'input' &&
          $prev_name !== '#text' &&
          $next_name !== '#text') ||
          $type === 'hidden'
          ) {
          $this->getInputAttributes($ele);
        }
      }
    }

  }

  /**
   * Helper method to get radio button attributes.
   */
  private function getRadioAttributes($ele) {
    // Find the name of this element. It comes from the parent's previous
    // sibling's child's value. I think that's some kind of cousin.
    $radio_name = $ele->parentNode->previousSibling->firstChild->nodeValue;
    $input_name = $ele->attributes->getNamedItem('name')->value;
    // Get all of the radio children. It's like a form's birthday party, but
    // without the creepy clown.
    $radios = $ele->parentNode->childNodes;
    $radio_array = [
      '#type' => 'radios',
      '#title' => $radio_name,
      '#options' => [],
      '#options_display' => 'side_by_side',
    ];
    // Cycle through and get all of the radios within this group with the same
    // name.
    foreach ($radios as $radio) {
      if ($radio->nodeName === 'input') {
        $radio_value = $radio->attributes->getNamedItem('value')->value;
        $radio_label = $radio->nextSibling->nodeValue;
        $radio_array['#options'][$radio_value] = $radio_label;
      }
    }
    // Put it on the form.
    $this->elementArray[$input_name] = $radio_array;

  }

  /**
   * Helper method to parse the HTML attributes.
   *
   *   The current HTML tag.
   *
   * @param \DOMNode $ele
   *   The attributes of the current tag.
   */
  private function getInputAttributes(\DOMNode $ele) {
    $tag = $ele->tagName;
    $attributes = $ele->attributes;
    // These are specific tags that are special.
    $attribute_array = ['name', 'label', 'class'];
    $type_tags = ['select', 'input'];
    $sibling_tags = ['div', 'label'];
    // Try and find a label for this and use it.  We have to do some looking
    // around.
    if (in_array($tag, $type_tags) && isset($ele->previousSibling->firstChild) && in_array($ele->previousSibling->firstChild->tagName, $sibling_tags)) {
      $tag_label = trim($ele->previousSibling->firstChild->nodeValue);
    }
    // The parent 'form' tag is special and needs some extra attention to make
    // sure we get the method and action right.
    if ($tag === 'form') {
      $this->getFormAttributes($attributes);
    }

    // We want the markup to go directly below the form attributes so that the
    // description shows at the top of the form.
    $this->getMarkupElement();

    // Go through all of the attributes of the tag and sort them out in key
    // value pairs in order to build the YAML.
    foreach ($attributes as $attr) {
      $name = $attr->name;
      $value = $attr->value;

      // Drupal is picky and a text field is not a textfield.  Let's fix that.
      $value = ($value === 'text' ? 'textfield' : $value);
      if (!in_array($name, $attribute_array)) {
        $input_attrs['#' . $name] = $value;
      }
      if ($name === 'name') {
        $input_name = $value;
      }
      // Classes need to go into their own array for the YAML.
      if ($name === 'class') {
        $class_array = explode(' ', $value);
        $input_attrs['#attributes']['class'] = $class_array;
      }
      // Checkboxes are inside their labels because Watson. It won Jeopardy, but
      // can't figure out how to do things the right way.
      if ($name === 'type' && $value === 'checkbox') {
        $tag_label = $this->element->parentNode->nodeValue;
      }
      // Let's just make everything except the checkboxes required.
      // @TODO once we have a form with an 'opt in' check, see what the ID is
      // to make that required as well.
      if ($name === 'type' && $value !== 'checkbox' && in_array($tag, $type_tags)) {
        $input_attrs['#required'] = TRUE;
      }
    }
    // The tag label becomes the field title in Webform.
    if (isset($tag_label)) {
      $input_attrs['#title'] = str_replace('*', '', $tag_label);
    }
    // Special little helper to get all of the options from a select list.
    if ($tag === 'select') {
      $this->getSelectOptions($input_attrs, $ele);
    }
    // Check if we're working with an email field.
    // @TODO: Verify other control columns that are constants
    if (isset($input_attrs['#id']) && $input_attrs['#id'] === 'control_EMAIL') {
      $input_attrs['#type'] = 'email';
    }
    // Now put it all together into one big array.
    if (isset($input_name)) {
      $this->elementArray[$input_name] = $input_attrs;
    }
  }

  /**
   * Helper method to create the webform.
   */
  public function createWebform() {
    // Append default settings.
    $settings = Webform::getDefaultSettings();
    // Set a default message on the confirmation page.
    $settings['confirmation_title'] = $this->t('Thank you');
    $settings['confirmation_message'] = $this->t('Your submission was successful.');
    // This prevents submissions from being saved to the Drupal database.
    $settings['results_disabled'] = TRUE;
    // Allow form to populate with URL parameters.
    $settings['form_prepopulate'] = TRUE;

    $form_exists = $this->doesWebformExist();
    // Prepare the remote_post handler.
    $this->prepHandler();
    // Check if we're in the C or the U of CRUD.
    if ($form_exists) {
      // Update here.
      $webform = Webform::load($this->webformId);
      $old_form = $webform->getElementsDecoded();
      // Compare the old and new on update.
      $this->oldFormFlat = $this->flattenArray($old_form, []);

      $this->newFormFlat = array_merge($this->preArray, $this->elementArray);

      $form_diff_new_to_old = $this->getDiffOfForms($this->newFormFlat, $this->oldFormFlat);
      $form_diff_old_to_new = $this->getDiffOfForms($this->oldFormFlat, $this->newFormFlat);

      $this->updateFormFromDiff($form_diff_old_to_new, $form_diff_new_to_old);

      // Get the element array and then merge it with the form attributes array.
      $this->setTwoColumnLayout();
      $this->formArray = array_merge($this->preArray, $this->layoutArray);
      $webform->set('title', $this->title)
        ->set('description', $this->adminDescription)
        ->setElements($this->formArray);
      // The handler should already be attached on an existing form.
      $webform->setStatus(TRUE);
      $webform->save();
    }
    else {
      $this->setTwoColumnLayout();
      $this->formArray = array_merge($this->preArray, $this->layoutArray);
      // Create a webform.
      $webform = Webform::create([
        'id' => $this->webformId,
        'title' => $this->title,
        'description' => $this->adminDescription,
        'elements' => $this->formArray,
        'settings' => $settings,
        'category' => 'Watson Form',
      ]);

      // Attach the handler and save the webform.
      $handler = $this->handlerManager->createInstance('remote_post', $this->handlerArray);
      $webform->setStatus(TRUE);
      $webform->addWebformHandler($handler);
    }

    return $webform;
  }

  /**
   * Helper function to check whether a matching Webform entity exists.
   */
  public function doesWebformExist() {
    if ($this->entity->getReferencedWebform() && $this->entity->getReferencedWebform()->get('id')) {
      return (bool) TRUE;
    }
    else {
      $entity = $this->entityTypeManager->getStorage('webform')->getQuery()
        ->condition('id', $this->webformId)
        ->execute();
      return (bool) $entity;
    }
    return FALSE;
  }

  /**
   * Get attributes of the form element.
   *
   * @param \DOMNamedNodeMap $attributes
   *   The attributes object.
   */
  public function getFormAttributes(\DOMNamedNodeMap $attributes) {
    // Remember when we decided the form tag was special? It even gets its own
    // helper function to parse the attributes out.
    foreach ($attributes as $attr) {
      $name = $attr->name;
      $value = $attr->value;
      if (!in_array($name, ['method', 'action'])) {
        $this->preArray['#attributes'][$name] = $value;
      }
      else {
        $this->preArray['#' . $name] = $value;
      }
    }
  }

  /**
   * Get the select list options.
   *
   * @param array $input_attrs
   *   The attributes array.
   * @param \DOMNode $ele
   *   The attributes array.
   */
  public function getSelectOptions(array &$input_attrs, \DOMNode $ele) {
    // Only do this on select inputs.
    $input_attrs['#type'] = 'select';
    // Make required by default.
    $input_attrs['#required'] = TRUE;
    // Get all of the options.
    $options = $ele->childNodes;
    if (count($options) > 0) {
      foreach ($options as $option) {
        // Cycle through the attributes to get the name and the values.
        foreach ($option->attributes as $attr) {
          $name = $attr->name;
          $value = $attr->value;
          if ($name === 'value') {
            if (strlen($value) > 0) {
              $options_array[$value] = $option->nodeValue;
            }
            else {
              // If no value is defined in the value attribute, set as default.
              $input_attrs['#empty_option'] = $option->nodeValue;
            }
          }
        }
      }
      $input_attrs['#options'] = $options_array;
    }
  }

  /**
   * Helper function to clean HTML.
   *
   * @param string $html
   *   Unsanitized HTML.
   */
  public function cleanHtml($html) {
    // Remove all tags that aren't these.
    $tags = '<input><form><div><select><option><label>';
    // Remove all newlines, just because.
    $html = preg_replace("/\r|\n|\r\n/", "", $html);
    $html = preg_replace('/\s+/', ' ', $html);
    // Clear whitespace between tags.
    $html = preg_replace('/(\>)\s*(\<)/m', '$1$2', $html);
    // Remove those pesky tags.
    $cleaned = strip_tags($html, $tags);
    $unstyled = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $cleaned);

    return $unstyled;
  }

  /**
   * Helper function to place the markup element.
   */
  public function getMarkupElement() {
    if (isset($this->formText)) {
      $this->preArray['markup'] = [
        '#type' => 'processed_text',
        '#text' => $this->formText,
        '#title' => 'Webform Description',
        '#format' => $this->formTextFormat,
        '#label_attributes' => [
          'class' => [
            'watson-form-label',
          ],
          'style' => 'display: none;',
        ],
      ];
    }
  }

  /**
   * Helper method to create two column layout.
   */
  public function setTwoColumnLayout() {
    $hidden_array = [];
    // Separate out the hidden from the regular.
    foreach ($this->elementArray as $name => $value) {
      if ($value['#type'] === 'hidden') {
        $hidden_array[$name] = $value;
        // Remove the hidden elements so they don't bork the layout.
        unset($this->elementArray[$name]);
      }
    }
    // For the sake of time, we're going to default to a 2 column layout.
    $element_count = count($this->elementArray);
    // If there's an odd number of elements, put more in the first row.
    $col_1_count = round($element_count / 2, 0, PHP_ROUND_HALF_UP);
    // This is the count left after removing the first section.
    $leftover_count = $element_count - $col_1_count;
    // Slice into two arrays.  One for each column.
    $col_1 = array_slice($this->elementArray, 0, $col_1_count, TRUE);
    $col_2 = array_slice($this->elementArray, -$leftover_count, NULL, TRUE);
    // Basically a template for the array.
    $array_with_layout = [
      'flexbox' => [
        '#type' => 'webform_flexbox',
        'column_1' => [
          '#type' => 'container',
        ],
        'column_2' => [
          '#type' => 'container',
        ],
      ],
    ];
    // Add each element into the correct column.
    foreach ($col_1 as $k => $v) {
      $array_with_layout['flexbox']['column_1'][$k] = $v;
    }
    foreach ($col_2 as $k => $v) {
      $array_with_layout['flexbox']['column_2'][$k] = $v;
    }
    // Make sure the hidden elements make it back into the form.
    foreach ($hidden_array as $k => $v) {
      $array_with_layout[$k] = $v;
    }
    // Set the element array.
    $this->layoutArray = $array_with_layout;
  }

  /**
   * Helper function to diff new and old forms.
   *
   * @param array $form_1
   *   A form array.
   * @param array $form_2
   *   A the form array to be compared.
   */
  public function getDiffOfForms(array $form_1, array $form_2) {
    $difference = [];

    foreach ($form_2 as $key => $value) {
      if (is_array($value)) {
        if (!isset($form_1[$key]) || !is_array($form_1[$key])) {
          $difference[$key] = $value;
        }
        else {
          $new_diff = $this->getDiffOfForms($value, $form_1[$key]);
          if (!empty($new_diff)) {
            $difference[$key] = $new_diff;
          }
        }
      }
      elseif (!array_key_exists($key, $form_1) || $form_1[$key] !== $value) {
        $difference[$key] = $value;
      }
    }

    return $difference;
  }

  /**
   * Helper method to flatten the old form array for easier comparison.
   *
   * Modified from https://stackoverflow.com/questions/9546181/flatten-multidimensional-array-concatenating-keys/9546235
   *
   * @param array $array
   *   The old form array.
   */
  public function flattenArray(array $array) {
    $result = [];
    // Keys that we're keeping out of the flat array.
    $skip_keys = ['webform_flexbox', 'container'];
    // Recursively loop through the arrays until we find level just above the
    // leaves.
    foreach ($array as $key => $value) {
      if (is_array($value)) {
        if (isset($value['#type']) && in_array($value['#type'], $skip_keys)) {
          $result = $result + $this->flattenArray($value);
        }
        else {
          $result[$key] = $value;
        }
      }
      else {
        $result[$key] = $value;
      }
    }
    // Make sure that we don't accidentally re-use a type that didn't get
    // cleared on a previous loop.
    if (isset($result['#type']) && $result['#type'] == 'webform_flexbox') {
      unset($result['#type']);
    }

    return $result;
  }

  /**
   * Helper method to update the Webform.
   *
   * @param array $old_to_new
   *   The diff array from old to new.
   * @param array $new_to_old
   *   The diff array from new to old.
   */
  public function updateFormFromDiff(array $old_to_new, array $new_to_old) {
    // The keys that we're going to be holding onto as part of the pre-array or
    // elements outside the layout containers.
    $pre_keys = ['markup', '#method', '#attributes', '#action'];
    foreach ($old_to_new as $key => $value) {
      // Merge in any changes to the new form preArray.
      if (in_array($key, $pre_keys)) {
        $this->preArray[$key] = array_merge($this->preArray[$key], $old_to_new[$key]);

        if ($key === 'markup') {
          $this->entity->set('form_text', $value['#text']);
        }
      }
      // Merge in changes to the main form section.
      elseif (isset($this->elementArray[$key])) {
        $this->elementArray[$key] = array_merge($this->elementArray[$key], $old_to_new[$key]);
      }
    }
    // If an element doesn't exist in the old, add it in.
    foreach ($new_to_old as $key => $value) {
      if (in_array($key, $pre_keys)) {
        if (isset($this->preArray[$key])) {
          $this->preArray[$key] = array_merge($this->preArray[$key], $new_to_old[$key]);
        }
        else {
          $this->preArray[$key] = $new_to_old[$key];
        }
      }
      if (!isset($this->elementArray[$key])) {
        $this->elementArray[$key] = $new_to_old[$key];
      }
    }
  }

  /**
   * Helper method to get checkbox items and collections.
   */
  public function getCheckboxItems($ele) {
    // Checkboxes can come in collections or one-offs.  This will cover both.
    $checkbox_name = $ele->getAttribute('name');
    $checkbox_label = $ele->getAttribute('label');
    $checkbox_id = $ele->getAttribute('id');
    $options_array = [];
    // Outer if to check if we've already set this key.
    if (!isset($this->elementArray[$checkbox_name])) {
      $checkboxes = $this->xpathObject->query('//input[@name=\'' . $checkbox_name . '\']');
      // Logic for group of checkboxes.
      if ($checkboxes->length > 1) {
        foreach ($checkboxes as $v) {
          $value = $v->getAttribute('value');
          $options_array[$value] = $value;
        }
        $this->elementArray[$checkbox_name] = [
          '#type' => 'checkboxes',
          '#id' => $checkbox_id,
          '#title' => $checkbox_label,
          '#options' => $options_array,
          '#wrapper_attributes' => [
            'id' => 'container_' . $checkbox_name,
          ],
        ];
      }
      // Logic for single checkbox.
      else {
        $checkbox_value = $ele->getAttribute('value');
        $this->elementArray[$checkbox_name] = [
          '#type' => 'checkbox',
          '#id' => $checkbox_id,
          '#title' => $checkbox_label,
          '#wrapper_attributes' => [
            'id' => 'container_' . $checkbox_name,
          ],
        ];
        if (!empty($checkbox_value)) {
          $this->elementArray[$checkbox_name]['#value'] = $checkbox_value;
        }
      }
    }
  }

  /**
   * Helper method to prep the default handler.
   */
  public function prepHandler() {
    // Only run this if we have a set action.
    if (in_array('#action', array_keys($this->preArray))) {
      $action = $this->preArray['#action'];
      $method = $this->preArray['#method'];
      // Remove the set action and method from the preArray so it doesn't make
      // it to the form itself.
      unset($this->preArray['#action']);
      unset($this->preArray['#method']);
    }
    if (!empty($action)) {
      // Create the settings for the handler.
      $handler_array = [
        'id' => 'remote_post',
        'label' => 'Remote post',
        'handler_id' => 'remote_post',
        'status' => TRUE,
        'conditions' => [],
        'weight' => 0,
        'settings' => [
          'method' => strtoupper($method),
          'type' => 'x-www-form-urlencoded',
          'excluded_data' => [
            'serial' => 'serial',
            'sid' => 'sid',
            'uuid' => 'uuid',
            'token' => 'token',
            'uri' => 'uri',
            'created' => 'created',
            'completed' => 'completed',
            'changed' => 'changed',
            'in_draft' => 'in_draft',
            'current_page' => 'current_page',
            'remote_addr' => 'remote_addr',
            'uid' => 'uid',
            'langcode' => 'langcode',
            'webform_id' => 'webform_id',
            'entity_type' => 'entity_type',
            'entity_id' => 'entity_id',
            'locked' => 'locked',
            'sticky' => 'sticky',
            'notes' => 'notes',
          ],
          'completed_url' => $action,
        ],
      ];
    }
    $this->handlerArray = $handler_array;
  }

  /**
   * Get the form HTML.
   */
  public function getHtml() {
    // Get the raw HTML from the field. In the words of ODB, Ooh baby I like it
    // raw.
    return $this->entity->getFormHtml()->value;
  }

  /**
   * Returns the element array.
   *
   * @return array
   *   The element array.
   */
  public function getElementArray() {
    return $this->elementArray;
  }

  /**
   * Helper method to get the format of a field.
   *
   * @param array $form_text
   *   The field array.
   *
   * @return string
   *   The format of the field.
   */
  public function getFormTextFormat(array $form_text) {
    foreach ($form_text['widget'] as $k => $v) {
      if (!is_numeric($k)) {
        continue;
      }
      $format = $v['#format'];

      return $format;
    }

  }

}
