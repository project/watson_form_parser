<?php

namespace Drupal\watson_form_parser\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that there is only one form in the HTML and that it is the parent.
 *
 * @Constraint(
 *   id = "SingleForm",
 *   label = @Translation("Ensure a single form is submitted and that it is the parent.")
 * )
 */
class SingleForm extends Constraint {

  public $singleForm = "There can only be one form in the submitted HTML. %value forms found.";

  public $parentForm = "There can be no form elements outside of the form tag. %value found";

}
