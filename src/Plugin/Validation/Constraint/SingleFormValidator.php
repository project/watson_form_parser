<?php

namespace Drupal\watson_form_parser\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\watson_form_parser\Form\WatsonFormEntityForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Validates the SingleForm constraint.
 */
class SingleFormValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\watson_form_parser\Form\WatsonFormEntityForm
   */
  protected $entityForm;

  /**
   * Constructs an Entity Form object.
   *
   * @param Drupal\watson_form_parser\Form\WatsonFormEntityForm $entity_form
   *   The entity form.
   */
  public function __construct(WatsonFormEntityForm $entity_form) {
    $this->entityForm = $entity_form;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('watson_form_entity.form')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    foreach ($items as $item) {
      $html = $item->value;

      $clean_html = $this->entityForm->cleanHtml($html);

      $dom = new \DOMDocument();
      $dom->loadHTML($clean_html, LIBXML_HTML_NOIMPLIED);
      $eles = $dom->getElementsByTagName('*');
      $forms = $dom->getElementsByTagName('form');
      $parent = $eles[0]->tagName;

      if (count($forms) > 1) {
        $this->context->addViolation($constraint->singleForm, ['%value' => count($forms)]);
      }

      if ($parent !== 'form') {
        $this->context->addViolation($constraint->parentForm, ['%value' => $parent]);
      }
    }
  }

}
