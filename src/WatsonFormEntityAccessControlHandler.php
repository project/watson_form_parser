<?php

namespace Drupal\watson_form_parser;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Watson form entity.
 *
 * @see \Drupal\watson_form_parser\Entity\WatsonFormEntity.
 */
class WatsonFormEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\watson_form_parser\Entity\WatsonFormEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished watson form entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published watson form entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit watson form entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete watson form entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add watson form entities');
  }

}
