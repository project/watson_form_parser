<?php

namespace Drupal\watson_form_parser;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines a class to build a listing of Watson form entities.
 *
 * @ingroup watson_form_parser
 */
class WatsonFormEntityListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new WatsonFormListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type, $storage);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Watson form ID');
    $header['name'] = $this->t('Name');
    $header['webform'] = $this->t('Linked Webform');
    $header['description'] = $this->t('Webform description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    if ($referenced_form = $entity->get('referenced_webform')->entity) {
      $referenced_form_id = $referenced_form->id() ?: NULL;
      $referenced_form_label = $referenced_form->label() ?: NULL;
      $referenced_form_description = $referenced_form->get('description') ? strip_tags($referenced_form->get('description')) : NULL;
      $referenced_form_link = Link::createFromRoute(
        $referenced_form_label,
        'entity.webform.canonical',
        ['webform' => $referenced_form_id]
      );
    }

    /* @var $entity \Drupal\watson_form_parser\Entity\WatsonFormEntity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.watson_form_entity.edit_form',
      ['watson_form_entity' => $entity->id()]
    );
    $row['webform'] = $referenced_form_link ?? NULL;
    $row['description'] = $referenced_form_description ?? NULL;

    return $row + parent::buildRow($entity);
  }

}
