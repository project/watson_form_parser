<?php

namespace Drupal\watson_form_parser;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\watson_form_parser\Entity\WatsonFormEntityInterface;

/**
 * Defines the storage handler class for Watson form entities.
 *
 * This extends the base storage class, adding required special handling for
 * Watson form entities.
 *
 * @ingroup watson_form_parser
 */
class WatsonFormEntityStorage extends SqlContentEntityStorage implements WatsonFormEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(WatsonFormEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {watson_form_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {watson_form_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(WatsonFormEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {watson_form_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('watson_form_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
