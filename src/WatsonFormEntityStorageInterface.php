<?php

namespace Drupal\watson_form_parser;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\watson_form_parser\Entity\WatsonFormEntityInterface;

/**
 * Defines the storage handler class for Watson form entities.
 *
 * This extends the base storage class, adding required special handling for
 * Watson form entities.
 *
 * @ingroup watson_form_parser
 */
interface WatsonFormEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Watson form revision IDs for a specific Watson form.
   *
   * @param \Drupal\watson_form_parser\Entity\WatsonFormEntityInterface $entity
   *   The Watson form entity.
   *
   * @return int[]
   *   Watson form revision IDs (in ascending order).
   */
  public function revisionIds(WatsonFormEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Watson form author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Watson form revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\watson_form_parser\Entity\WatsonFormEntityInterface $entity
   *   The Watson form entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(WatsonFormEntityInterface $entity);

  /**
   * Unsets the language for all Watson form with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
