<?php

namespace Drupal\watson_form_parser;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for watson_form_entity.
 */
class WatsonFormEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
