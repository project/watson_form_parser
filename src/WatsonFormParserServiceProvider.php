<?php

namespace Drupal\watson_form_parser;

use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;

/**
 * Replace Webform validator service.
 */
class WatsonFormParserServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('webform.elements_validator');
    $definition->setClass('Drupal\watson_form_parser\WatsonFormWebformEntityElementsValidator');
  }

}
