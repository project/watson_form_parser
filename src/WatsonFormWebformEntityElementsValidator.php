<?php

namespace Drupal\watson_form_parser;

use Drupal\Core\Serialization\Yaml;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformEntityElementsValidator;

/**
 * Webform elements validator.
 */
class WatsonFormWebformEntityElementsValidator extends WebformEntityElementsValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(WebformInterface $webform, array $options = []) {
    $options += [
      'required' => TRUE,
      'yaml' => TRUE,
      'array' => TRUE,
      'properties' => TRUE,
      'submissions' => TRUE,
      'hierarchy' => TRUE,
      'rendering' => TRUE,
      'duplicate' => TRUE,
    ];

    $webform_id = $webform->id();

    $query = $this->entityTypeManager->getStorage('watson_form_entity')->getQuery();

    $watson_form = $query->condition('referenced_webform', $webform_id)
      ->execute();

    if (empty($watson_form)) {
      return parent::validate($webform);
    }
    else {

      $this->webform = $webform;

      $this->elementsRaw = $webform->getElementsRaw();
      $this->originalElementsRaw = $webform->getElementsOriginalRaw();

      // Validate required.
      if ($options['required'] && ($message = $this->validateRequired())) {
        return [$message];
      }

      // Validate contain valid YAML.
      if ($options['yaml'] && ($message = $this->validateYaml())) {
        return [$message];
      }

      $this->elements = Yaml::decode($this->elementsRaw);
      $this->originalElements = Yaml::decode($this->originalElementsRaw);

      $this->elementKeys = [];
      if (is_array($this->elements)) {
        $this->getElementKeysRecursive($this->elements, $this->elementKeys);
      }

      // Validate elements are an array.
      if ($options['array'] && ($message = $this->validateArray())) {
        return [$message];
      }

      // Validate duplicate element name.
      if ($options['duplicate']) {
        if ($messages = $this->validateDuplicateNames()) {
          return $messages;
        }
      }

      // Validate ignored properties.
      if ($options['properties'] && ($messages = $this->validateProperties())) {
        return $messages;
      }

      // Validate submission data.
      if ($options['submissions'] && ($messages = $this->validateSubmissions())) {
        return $messages;
      }

      // Validate hierarchy.
      if ($options['hierarchy'] && ($messages = $this->validateHierarchy())) {
        return $messages;
      }

      // Validate rendering.
      if ($options['rendering'] && ($message = $this->validateRendering())) {
        return [$message];
      }
      return NULL;
    }
  }

}
