<?php

namespace Drupal\Tests\watson_form_parser\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\watson_form_parser\Form\WatsonFormEntityForm;
use Drupal\webform\Plugin\WebformHandlerManagerInterface;

/**
 * Test description.
 *
 * @group watson_form_parser
 *
 * @coversDefaultClass Drupal\watson_form_parser\Form\WatsonFormEntityForm
 */
class WatsonFormEntityFormTest extends UnitTestCase {

  /**
   * The handler manager.
   *
   * @var \Drupal\webform\Plugin\WebformHandlerManagerInterface
   */
  protected $mockHandlerManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $mockEntityTypeBundleInfo;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $mockTime;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $mockEntityRepository;

  /**
   * The account proxy/current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $mockCurrentUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->mockEntityRepository = $this->prophesize(EntityRepositoryInterface::class);
    $this->mockEntityRepository->willImplement('Drupal\Core\Entity\EntityRepositoryInterface');

    $this->mockEntityTypeBundleInfo = $this->prophesize(EntityTypeBundleInfoInterface::class);
    $this->mockEntityTypeBundleInfo->willImplement('Drupal\Core\Entity\EntityTypeBundleInfoInterface');

    $this->mockTime = $this->prophesize(TimeInterface::class);
    $this->mockTime->willImplement('Drupal\Component\Datetime\TimeInterface');

    $this->mockHandlerManager = $this->prophesize(WebformHandlerManagerInterface::class);
    $this->mockHandlerManager->willImplement('Drupal\webform\Plugin\WebformHandlerManagerInterface');

    $this->mockCurrentUser = $this->prophesize(AccountProxyInterface::class);
    $this->mockCurrentUser->willImplement('Drupal\Core\Session\AccountProxyInterface');

  }

  /**
   * Tests constructor.
   *
   * @covers ::__construct
   */
  public function testConstructor() {
    $form_parser = new WatsonFormEntityForm(
      $this->mockEntityRepository->reveal(),
      $this->mockEntityTypeBundleInfo->reveal(),
      $this->mockTime->reveal(),
      $this->mockHandlerManager->reveal(),
      $this->mockCurrentUser->reveal()
    );

    $this->assertInstanceOf('Drupal\watson_form_parser\Form\WatsonFormEntityForm', $form_parser);
  }

  /**
   * Test Html cleaning.
   *
   * @covers ::cleanHtml
   */
  public function testCleanHtml() {
    $form_parser = $this->getMockFormParser();

    $this->cleanHtml = $form_parser->cleanHtml($this->getMockHtml());

    $this->assertNotContains('<table', $this->cleanHtml, 'Does contain tables.');
    $this->assertNotContains('<tr', $this->cleanHtml, 'Does contain <tr> tags.');
    $this->assertNotContains('<td', $this->cleanHtml, 'Does contain <td> tags.');
    $this->assertNotContains('style=', $this->cleanHtml, 'Does contain inline styles.');
    $this->assertNotRegExp('/\r|\n|\r\n/', $this->cleanHtml, 'Does contain newline characters.');
  }

  /**
   * Test Html cleaning.
   *
   * @covers ::cleanHtml
   */
  public function testGetRadioAttributes() {

    $form_parser = $this->getMockFormParser();
    $reflected_parser = new \ReflectionClass($form_parser);
    $reflected_method = $reflected_parser->getMethod('getRadioAttributes');
    $reflected_method->setAccessible(TRUE);

    $dom = new \DOMDocument();
    $dom->loadHTML($this->getMockRadio(), LIBXML_HTML_NOIMPLIED);
    $eles = $dom->getElementsByTagName('*');

    foreach ($eles as $ele) {
      if ($ele->hasAttributes()) {
        $attributes = $ele->attributes;
        $type = $attributes->getNamedItem('type')->value ?? NULL;
        // Radios are their own challenge, so they get their own method.
        if ($type === 'radio' && $ele === $ele->parentNode->firstChild) {
          $reflected_method->invoke($form_parser, $ele);
        }
      }
    }
    $element_array = $form_parser->getElementArray();
    $element_name = array_keys($element_array)[0];
    $this->assertArrayHasKey('RadioName', $element_array, 'Label not found');
    $this->assertArrayHasKey('#type', $element_array[$element_name], 'Type attribute not found.');
    $this->assertEquals('radios', $element_array[$element_name]['#type'], 'Not radio type.');
    $this->assertArrayHasKey('#title', $element_array[$element_name], 'Title attribute not found.');
    $this->assertEquals('Test Radios', $element_array[$element_name]['#title'], 'Title not found.');
    $this->assertArrayHasKey('#options', $element_array[$element_name], 'Options attribute not found.');
    $this->assertArrayHasKey('Yes', $element_array[$element_name]['#options'], 'Options not found.');
    $this->assertArrayHasKey('No', $element_array[$element_name]['#options'], 'Options not found.');
    $this->assertArrayHasKey('#options_display', $element_array[$element_name], 'Display attribute not found.');
    $this->assertEquals('side_by_side', $element_array[$element_name]['#options_display'], 'Display setting not found.');
  }

  /**
   * Helper method to mock the HTML.
   *
   * @return string
   *   The test HTML string.
   */
  protected function getMockHtml() {
    $html = file_get_contents(__DIR__ . './../../fixtures/testRawHtml.html');
    return (string) $html;
  }

  /**
   * Creates mock radio HTML.
   *
   * @return string
   *   The radio input string.
   */
  protected function getMockRadio() {
    $radio = '<div><div>Test Radios</div></div><div><input type="radio" name="RadioName" id="Radio1" label="First Button" value="Yes">Yes<input type="radio" name="RadioName" id="Radio2" label="Second Button" value="No">No</div>';
    return $radio;
  }

  /**
   * Helper method to mock the form parser object for each test.
   *
   * @return object
   *   The form parser object.
   */
  protected function getMockFormParser() {
    $form_parser = new WatsonFormEntityForm(
      $this->mockEntityRepository->reveal(),
      $this->mockEntityTypeBundleInfo->reveal(),
      $this->mockTime->reveal(),
      $this->mockHandlerManager->reveal(),
      $this->mockCurrentUser->reveal()
    );
    return $form_parser;
  }

}
