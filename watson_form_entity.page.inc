<?php

/**
 * @file
 * Contains watson_form_entity.page.inc.
 *
 * Page callback for Watson form entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Watson form templates.
 *
 * Default template: watson_form_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_watson_form_entity(array &$variables) {
  // Fetch WatsonFormEntity Entity Object.
  $watson_form_entity = $variables['elements']['#watson_form_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
